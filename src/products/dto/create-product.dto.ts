import { IsNotEmpty, IsInt } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  name: string;
  @IsNotEmpty()
  @IsInt()
  price: number;
}
